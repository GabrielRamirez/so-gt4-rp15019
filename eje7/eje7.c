
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    int gradoPol, i, j;
    double *coeficientes, valordex, resultado=0;

    printf("Cual es el grado del polinomio? ");
    scanf("%d", &gradoPol);
    coeficientes = (double *)malloc((gradoPol+1) * sizeof(double));

    for (i = 0; i <= gradoPol; i++)
    {
        printf("Ingrese el siguiente coeficiente ");
        scanf("%lf", &coeficientes[i]);
    }
    printf("Escriba el valor a evaluar x = ");
    scanf("%lf", &valordex); 

    
    printf("El polinomio evaluado es: \n"); 
    for ( i = 0; i <= gradoPol; i++)
    {
        printf("%.2lfx%d", coeficientes[i], i);
        if (coeficientes[i]>0 && i>=0 && i<gradoPol )
        {
            printf("+");
        }
        

    }


    printf("\nEvaluado en x = %.2f \n", valordex);    
    for (i = 0; i <= gradoPol; i++)
    {
        resultado = resultado+(coeficientes[i]*pow(valordex, i));
        
    }

    printf("El resultado es: %.2lf", resultado); 

    return 0;

}

/*Evalua en un polinomio dado el grado y los coeficientes del polinomio 
*/