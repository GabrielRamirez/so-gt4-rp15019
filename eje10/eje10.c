#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

typedef struct
{
	char nombre[40];
	int dui;
	float sueldo;
} empleado;

int main(int argc, char const *argv[])
{
	empleado empleados[1000];
	bool siguiente = true;
	int contador = 0, opcion, i;

	while (siguiente)
	{
		printf("Ingrese el nombre: \n");
		scanf("%s", empleados[contador].nombre);
		printf("Ingrese el numero de dui: \n");
		scanf("%d", &empleados[contador].dui);
		printf("Ingrese el sueldo: \n");
		scanf("%f", &empleados[contador].sueldo);
		contador++;
		printf("Desea agregar otro empleado? \n 1-Si \n 2-No \n");
		printf("Elija un numero\n");
		scanf("%d", &opcion);

		switch (opcion)
		{
		case 1:
			siguiente = true;
			break;

		case 2:
			siguiente = false;
			break;

		default:
			printf("Opcion no valida");
			siguiente = false;
		}
	}

	void imprimir()
	{
		printf("hay %d registros\n", contador);
		for (i = 0; i < contador; i++)
		{
			printf("Empleado: #%d %s %d %.2f \n", i, empleados[i].nombre, empleados[i].dui, empleados[i].sueldo);
		}
	}

	void ordenar_por_campo_nombre()
	{
		printf("ordenado por campo 1: \n ");
		empleado auxiliar;
		for (int i = 0; i < contador; i++)
		{
			for (int j = 1; j < contador; j++)
			{
				if (strcmp(empleados[j - 1].nombre, empleados[j].nombre) > 0)
				{
					auxiliar = empleados[j];
					empleados[j] = empleados[j - 1];
					empleados[j - 1] = auxiliar;
				}
			}
		}
	}

	void ordenar_por_campo_sueldo(empleado empleados[], int contador)
	{
		empleado auxiliar;
		printf("ordenado por campo 2: \n ");

		for (int i = 0; i < contador; i++)
		{
			for (int j = 1; j < contador; j++)
			{
				if (empleados[j].sueldo > empleados[j - 1].sueldo)
				{
					auxiliar = empleados[j];
					empleados[j] = empleados[j - 1];
					empleados[j - 1] = auxiliar;
				}
			}
		}
	}

	void promedio_sueldo(empleado empleados[], int contador)
	{
		printf("Calculo de promedio de sueldo: \n ");

		double res = 0;
		for (int i = 0; i < contador; i++)
		{
			res += empleados[i].sueldo;
		}
		printf("El promedio de los sueldos es: %0.2f\n", res / contador);
	}

	ordenar_por_campo_nombre();
	imprimir();
	ordenar_por_campo_sueldo(empleados, contador); 
	imprimir(); 
	promedio_sueldo(empleados, contador);

	return 0;
}
