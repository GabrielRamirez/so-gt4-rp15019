
#include <stdio.h>

int main()
{
    int *pnumero;
    int num1, num2;
    char *pchar;
    char letra1;
    num1 = 2;
    num2 = 5;
    letra1 = 'a';
    pnumero = &num1;

    printf("El valor de pnumero es: %p %d ", pnumero, *pnumero);

    return 0;
}

//pnumero Contiene la direccion de memoria de num1 y por lo tanto su contenido

