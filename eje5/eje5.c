
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    int cuantosnumeros, i, j, temp;
    int *numeros;

    printf("Cuantos numeros aleatorios desea obtener? ");
    scanf("%d", &cuantosnumeros);
    numeros = (int *)malloc(cuantosnumeros * sizeof(int));
    srand(time(NULL));
    for (i = 0; i < cuantosnumeros; i++)
    {
        numeros[i] = rand() % 100;
    }
    for (i = 0; i < (cuantosnumeros - 1); i++)
    {
        for (j = i + 1; j < cuantosnumeros; j++)
        {
            if (numeros[j] > numeros[i])
            {
                temp = numeros[j];
                numeros[j] = numeros[i];
                numeros[i] = temp;
            }
        }
    }

    i = 0;
    for (i = 0; i < cuantosnumeros; i++)
    {
        printf(" %d", numeros[i]);
    }

    return 0;
}

/*Calcula numeros de forma aleatoria en un rango de 
0 a 100 los ordena y los almacena en un arreglo con 
memoria dinamica usando malloc
*/