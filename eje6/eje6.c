
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    int cuantosnumeros, i, j, temp;
    float *numeros;
    float media, suma, max, min;

    printf("Cuantos numeros desea ingresar? ");
    scanf("%d", &cuantosnumeros);
    numeros = (float *)malloc(cuantosnumeros * sizeof(float));

    for (i = 0; i < cuantosnumeros; i++)
    {
        printf("Ingrese el siguiente numero ");
        scanf("%f", &numeros[i]);
    }

    max=numeros[0]; 
    min=numeros[0]; 
    for (i = 0; i < cuantosnumeros; i++)
    {
        suma = suma + numeros[i];
        if (max < numeros[i])
        {
            max = numeros[i];
        }
        if (min > numeros[i])
        {
            min = numeros[i];
        }
    }

    media = (float)(suma / cuantosnumeros);
    printf("La media aritmetica es: %.2f\n", media);
    printf("El mayor es: %.2f\n", max);
    printf("La menor es: %.2f\n", min);

    return 0;
}

/*Calcula la media ameritmetica el numero maximo y el numero minimo 
*/