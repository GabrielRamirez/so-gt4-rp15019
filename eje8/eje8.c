#include <stdio.h>
#include <stdlib.h>
#include <string.h>


char *pedirTexto();
void contarVocales(char *, int[]);
void imprimir(int[]);

void main(){
char *texto;
int num[5];
texto=pedirTexto();
contarVocales(texto, num);
imprimir(num);
}

// pide un texto al usuario por consola 
char *pedirTexto(){
char *msj=(char *)malloc(sizeof(char));
printf("Ingrese texto\n");
fgets(msj, 240, stdin);
printf("El mensaje es: %s", msj);
return msj;
}

void contarVocales(char *texto, int num[]){
int i; 
for (i = 0; i < 5; i++)
{
    num[i] =0; 
}


for(int i=0;  texto[i] != '\0'; i++){

if (texto[i]=='a' || texto[i] == 'A')
num[0]++; 

if (texto[i]=='e' || texto[i] == 'E')
num[1]++; 

if (texto[i]=='i' || texto[i] == 'I')
num[2]++; 

if (texto[i]=='o' || texto[i] == 'O')
num[3]++; 

if (texto[i]=='u' || texto[i] == 'U')
num[4]++; 

}

}

void imprimir(int num[]){
printf("vocal a:%d\n", num[0]);
printf("vocal e:%d\n", num[1]);
printf("vocal i:%d\n", num[2]);
printf("vocal o:%d\n", num[3]);
printf("vocal u:%d\n", num[4]);

}

// Cuenta las vocales que contiene un texto 